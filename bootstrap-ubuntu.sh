#! /bin/bash

set -e -x
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get install --assume-yes --no-install-recommends \
        docker.io \
        kexec-tools
