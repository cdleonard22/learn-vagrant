import os
import subprocess
from logging import getLogger

import pytest

logger = getLogger(__name__)


@pytest.fixture(scope="session")
def vagrant():
    try:
        subprocess.run(["vagrant", "up"], check=True)
        yield
    finally:
        if os.getenv("LEARN_VAGRANT_HALT", "1") == "0":
            logger.info("Skipped vagrant halt because of LEARN_VAGRANT_HALT")
        else:
            subprocess.run(["vagrant", "halt"], check=True)


def test_ssh_hello(vagrant):
    script = "echo $((1 + 2))"
    res = subprocess.run(
        ["vagrant", "ssh", "-c", script],
        check=True,
        text=True,
        capture_output=True,
    )
    assert res.stdout.strip() == "3"


def test_kexec(vagrant):
    script = "which kexec"
    res = subprocess.check_output(
        ["vagrant", "ssh", "-c", script],
        text=True,
    )
    assert res.strip() == "/sbin/kexec"


def test_ubuntu_osrel(vagrant):
    script = "cat /etc/os-release"
    res = subprocess.check_output(
        ["vagrant", "ssh", "ubuntu", "-c", script],
        text=True,
    )
    assert "ID=ubuntu" in res


def test_fedora_osrel(vagrant):
    script = "cat /etc/os-release"
    res = subprocess.check_output(
        ["vagrant", "ssh", "fedora", "-c", script],
        text=True,
    )
    assert "ID=fedora" in res
